﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DomainRoute.Startup))]
namespace DomainRoute
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
