﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DomainRoute
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.Add("company", new DomainRoute(
                domain: "*company-rb.{whatever}",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Company", action = "Index", id = UrlParameter.Optional }
            ));

            routes.Add("reflex", new DomainRoute(
                domain: "*reflexrb.{whatever}",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Reflex", action = "Index", id = UrlParameter.Optional }
            ));
        }
    }
}
