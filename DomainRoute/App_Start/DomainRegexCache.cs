namespace DomainRoute
{
    using System.Collections.Concurrent;
    using System.Text.RegularExpressions;

    internal static class DomainRegexCache
    {
        // since we're often going to have the same pattern used in multiple routes, it's best to build just one regex per pattern
        private static ConcurrentDictionary<string, Regex> _domainRegexes = new ConcurrentDictionary<string, Regex>();

        internal static Regex CreateDomainRegex(string domain)
        {
            return _domainRegexes.GetOrAdd(domain, (d) =>
                {
                    d = d.Replace("/", @"\/")
                        .Replace(".", @"\.")
                        .Replace("-", @"\-")
                        .Replace("{", @"(?<")
                        .Replace("}", @">(?:[a-zA-Z0-9_-]+))");

                    return new Regex("^" + d + "$", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.ExplicitCapture);
                });
        }
    }
}