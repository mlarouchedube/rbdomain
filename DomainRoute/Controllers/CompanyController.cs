﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DomainRoute.Controllers
{
    public class ReflexController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "This is the Reflex Home Controller.";
            return View();
        }
    }
}